#include "MTASSteppingAction.hh"
#include "G4SteppingManager.hh"
#include "G4Step.hh"
#include "G4Event.hh"
#include "G4StepPoint.hh"
#include "G4EventManager.hh"

#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ios.hh"



#include "G4Track.hh"
#include "G4String.hh"
#include "MTASEventAction.hh"
#include <sstream>

#include <stdio.h>
#include <math.h>
#include <string>

MTASSteppingAction::MTASSteppingAction( )
{
}

MTASSteppingAction::~MTASSteppingAction()
{
}

void MTASSteppingAction::UserSteppingAction(const G4Step* theStep)
{
  G4StepPoint* preStepPoint = theStep->GetPreStepPoint();
  G4double light;
  
  
	G4TouchableHandle preStepTouch =preStepPoint->GetTouchableHandle();
	G4VPhysicalVolume* preStepPV = preStepTouch->GetVolume();
	G4LogicalVolume* preStepLV = preStepPV->GetLogicalVolume();
	std::string materialName = preStepLV ->GetMaterial()->GetName(); 
	
	 
	if(materialName == "SodiumIodide")
	{
		light = FindLight(theStep);
		//number of module
		if(preStepPV->GetName() == "NaI_Centre_Physical")
			lightProdInNaIMod[0] += light;
		else
		{
			G4int copyNumber = preStepTouch -> GetCopyNumber(4);//4 - grand grand grandmother
			if(copyNumber < 18)
				lightProdInNaIMod[copyNumber+1] += light;
			else
				std::cout<<"error: copy number = "<<copyNumber<<std::endl;
		}
	} 
	
	
	/*
	G4Track* track = theStep -> GetTrack();
	G4String particleName = track -> GetDefinition() -> GetParticleName();
	G4int stepNum = track -> GetCurrentStepNumber();
	if(materialName == "SodiumIodide" && strcmp(particleName, "e-") != 0)
	{
		G4int EventID = G4EventManager::GetEventManager()->GetConstCurrentEvent()->GetEventID();
		G4double DeltaEnergy = theStep -> GetDeltaEnergy();
		std::stringstream ss;
		ss << "event: " << EventID 
		<< " step " << stepNum 
		<< " particle: " << particleName //wyciągnij stąd rozpad
		<< " proces: " << theStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName() 
		<< " kin Was: " << theStep->GetPreStepPoint()->GetKineticEnergy ()
		<< " kin Is: " << theStep->GetPostStepPoint()->GetKineticEnergy () 
		<< " delta En: " << DeltaEnergy << std::endl;
		MTASEventAction::testOutput += ss.str();

	}*/
}



G4double MTASSteppingAction::FindLight(const G4Step* theStep)
{
	G4StepPoint* preStepPoint = theStep->GetPreStepPoint();
  G4double depEnergy = theStep->GetTotalEnergyDeposit(); 
	G4double preStepTotEnergy = preStepPoint -> GetKineticEnergy();
	
	/*G4int EventID = G4EventManager::GetEventManager()->GetConstCurrentEvent()->GetEventID();
	 G4Track* track = theStep -> GetTrack();
	G4int stepNum = track -> GetCurrentStepNumber();
	std::cout << "event: " <<  EventID
	<< " step: " << stepNum 
	<< " en dep: " << depEnergy/keV << std::endl;*/
	return (LightForumla(preStepTotEnergy*1000)-LightForumla(preStepTotEnergy*1000-depEnergy*1000));
}

G4double MTASSteppingAction::LightForumla(G4double E)
{
	G4double a1 = 1.6;
	G4double a2 = 0.058;
	G4double a3 = 0.58;
	G4double a4 = 0.0049;
	G4double a5 = 0.25;
	G4double a6 = 0.479;
	G4double a7 = 0.00494;

	G4double light = E* (a1*(1.- exp(-a2*E)) + a3*E + a4*E*E)/(a5 + a6*E + a7*E*E);
	return light;
	//return E;
}

std::vector <G4double> MTASSteppingAction::lightProdInNaIMod(19,0);








