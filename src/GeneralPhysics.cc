//
// $Id: GeneralPhysics.cc 2016-11-14 A.Fijalkowska
//
/// \file GeneralPhysics.cc
/// \brief Implementation of the GeneralPhysics class
/// Class taken from example optical/LXe/
//
#include "GeneralPhysics.hh"
#include <iomanip>



GeneralPhysics::GeneralPhysics(const G4String& name)
                     :  G4VPhysicsConstructor(name) {}



GeneralPhysics::~GeneralPhysics() {
  //fDecayProcess = NULL;
}

#include "G4ParticleDefinition.hh"
#include "G4ProcessManager.hh"

#include "G4Geantino.hh"
#include "G4ChargedGeantino.hh"
#include "G4GenericIon.hh"

#include "G4Decay.hh"
#include "G4RadioactiveDecay.hh"
//#include "G4Proton.hh"

void GeneralPhysics::ConstructParticle()
{
  // pseudo-particles
  G4Geantino::GeantinoDefinition();
  G4ChargedGeantino::ChargedGeantinoDefinition();

  G4GenericIon::GenericIonDefinition();
}

void GeneralPhysics::ConstructProcess()
{
  //G4RadioactiveDecay* fDecayProcess = new G4RadioactiveDecay();
   G4Decay* fDecayProcess = new G4Decay();
  // Add Decay Process
  aParticleIterator->reset();
  while( (*aParticleIterator)() ){
    G4ParticleDefinition* particle = aParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    if (fDecayProcess->IsApplicable(*particle)) {
      pmanager ->AddProcess(fDecayProcess);
      // set ordering for PostStepDoIt and AtRestDoIt
      pmanager ->SetProcessOrdering(fDecayProcess, idxPostStep);
      pmanager ->SetProcessOrdering(fDecayProcess, idxAtRest);
    }
  }
}
